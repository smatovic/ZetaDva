/*
  Name:         Zeta Dva
  Description:  Amateur level chess engine
  Author:       Srdja Matovic <s.matovic@app26.de>
  Created at:   2011-01-15
  Updated at:   2021-08-29
  License:      MIT

  Copyright (c) 2011-2021 Srdja Matovic

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

#ifndef ZETA_H_INCLUDED
#define ZETA_H_INCLUDED

/* global variables */
extern FILE 	*LogFile;
/* counters */
extern u64 NODECOUNT;
extern u64 MOVECOUNT;
extern u64 TTHITS;
extern u64 COUNTERS1;
extern u64 COUNTERS2;
extern u64 BENCHNODECOUNT;
extern double BENCHEBF;
extern double BENCHTIME;
/* timers */
extern double start;
extern double end;
extern double elapsed;
extern bool TIMEOUT;  /* global value for time control*/
/* game state */
extern bool STM;
extern s32 SD;
extern s32 GAMEPLY;
extern s32 PLY;
extern Move *MoveHistory;
extern Hash *HashHistory;
extern Move *Killers;
extern Move *Counters;
extern const Bitboard LRANK[2];
extern double MaxTime;

extern bool xboard_post;
extern bool xboard_mode;
extern bool xboard_debug;

bool squareunderattack(Bitboard *board, bool stm, Square sq);
bool kingincheck(Bitboard *board, bool stm);
int cmp_move_desc(const void *ap, const void *bp);
void domove(Bitboard *board, Move move);
void undomove(Bitboard *board, Move move, Move lastmove, Cr cr, Hash hash);
void domovequick (Bitboard *board, Move move);
void undomovequick (Bitboard *board, Move move);
void donullmove(Bitboard *board);
void undonullmove(Bitboard *board, Move lastmove, Hash hash);
void printboard(Bitboard *board);
void printbitboard(Bitboard board);
void printmove(Move move);
void printmovecan(Move move);
Hash computehash(Bitboard *board, bool stm);
void save_to_tt(Hash hash, TTMove move, Score score, u8 flag, u8 depth);
struct TTE *load_from_tt(Hash hash);
s32 collect_pv_from_hash(Bitboard *board, Hash hash, Move *moves, s32 ply);
void save_killer(Move move, Score score, s32 ply);
bool isvalid(Bitboard *board);

#endif /* ZETA_H_INCLUDED */

